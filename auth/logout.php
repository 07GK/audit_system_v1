<?php
include '../class/class.php';

session_destroy();
unset($_COOKIE['remember_me']);
setcookie('remember_me', '', time() - 3600, '/'); 
setcookie('user_data', json_encode($user_data), time() - 3600,'/');
header('location:'.BASE_URL);

?>