<?php
if ($_SERVER['SERVER_NAME'] == 'localhost') {    
    define('BASE_URL', 'http://localhost/audit_system_core/' );
    define('UPLOAD_URL','assets/uploads/' );
    define('FILE_URL', dirname(__FILE__, 2) . '/' );

    define( 'DB_USER', 'root' );
    define( 'DB_PASS', '' );
    define( 'DB_NAME', 'audit_system' );
    define( 'DB_HOST', 'localhost' );
    define( 'DB_ENCODING', '' );
    
} else { //Database Connection for Live Hosting
    define('USER_URL', '');
    define('ADMIN_URL', '');
    define('UPLOAD', '');
    $host = "localhost:3306";
    $user = "";
    $pwd = "";
    $db = "";
}

date_default_timezone_set('Asia/Calcutta');
