<?php
include '../class/class.php';
if (!is_user_logged_in() || !is_admin()) {
  header('location:' . BASE_URL . 'index.php');
}
$storesData = $db->get_all("select * from stores where is_active = 1");
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Manage Remarks</title>
  <?php
  include '../includes/include-css.php';
  ?>
</head>

<body class="hold-transition sidebar-mini layout-fixed ">
  <div class=" wrapper ">
    <?php include '../includes/sidebar.php';
    include '../includes/navbar.php';
    ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0">View Remarks Details</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active">Remarks</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->
      <!-- Main content -->
      <section class="content">
        <div class="container-fluid">
          <div class='row'>
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="card card-primary">
                <div class="card-body">
                  <div class="col-md-12 mb-3 text-right">
                    <a href="<?= BASE_URL . 'admin/add_remark.php' ?>" class="d-inline btn btn-block btn-outline-primary col-md-2">Add Remark</a>
                  </div>
                  <div class="row">
                  <div class="form-group col-md-3">
                    <label for="selected_stores">Select Stores</label>
                    <select class="form-control multipleSelect select_store" id='selected_stores' data-placeholder='Search and select store' data-method="get_store_wise_user">
                      <?php
                      if (!empty($storesData)) {
                        echo '<option value="all">All</option>';
                        foreach ($storesData as $row) {
                          echo '<option value=' . $row['id'] . '>' . htmlspecialchars($row['name'], ENT_QUOTES, 'UTF-8') . '</option>';
                        }
                      } else {
                        echo '<option>No Stores Are Available</option>';
                      }
                      ?>
                    </select>
                  </div>
                  <div class="form-group col-md-3 d-flex align-items-center pt-4">
                    <button type="button" class="btn btn-outline-primary btn-sm" onclick="date_wise_search()">Filter</button>                    
                  </div>
                  </div>
                  <!-- Table -->
                  <table id='storeTable' class='display dataTable' data-url='apis/get_remark_details.php'>

                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Store Name</th>
                        <th>Remark</th>
                        <th>Operate</th>
                      </tr>
                    </thead>

                  </table>
                </div>
              </div>
              <!-- /.card -->
            </div>
          </div>
        </div>
      </section>
    </div>
    <!-- /.content-wrapper -->
  </div>
  <?php include '../includes/footer.php'; ?>
</body>
<?php include '../includes/include-script.php'; ?>

</html>