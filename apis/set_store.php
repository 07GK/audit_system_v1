<?php
include '../class/class.php';
if(!is_user_logged_in()){
    move( BASE_URL . 'index.php');
}

if(isset($_POST)){        
    if($_POST['method']=='add'){
        if(isset($_POST['edit_id'])){
            $_POST['edit_id'] = $db->decrypt($_POST['edit_id']);
            if($db->is_exist('stores',['name'=>trim($_POST['name']),'is_active'=>1],trim($_POST['edit_id']))){ 
                $response['error'] = true;
                $response['message'] = 'Store name already exist!';
                header('Content-Type: application/json');
                echo json_encode($response);
                return false;
            }
        }else{
            if($db->is_exist('stores',['name'=>trim($_POST['name']),'is_active'=>1])){ 
                $response['error'] = true;
                $response['message'] = 'Store name already exist!';
                header('Content-Type: application/json');
                echo json_encode($response);
                return false;
            }
        }

        $flag=0;
        if(empty(trim($_POST['name'])) || empty(trim($_POST['address']))){
            $flag=1;
        }   

        if($flag==0){

            $tmp = [
                'name'=> filter_var($_POST['name'], FILTER_SANITIZE_STRING),
                'address'=> filter_var($_POST['address'], FILTER_SANITIZE_STRING),
            ];    
            
            if(isset($_POST['edit_id'])){ 
                $_POST['edit_id'] = filter_var($_POST['edit_id'], FILTER_SANITIZE_STRING);
                $db->update('stores',$tmp,['id'=>$_POST['edit_id']]);
                $id = $_POST['edit_id'];
            }else{        
                $id = $db->insert('stores',$tmp);                
            }            
            if(isset($_POST['selectedUsers'])){   

                $user_store_conj = [];            
                foreach ($_POST['selectedUsers'] as $val) {                
                    array_push($user_store_conj,['store_id'=>$id,'user_id'=>$val]);    
                }                
                
                if(isset($_POST['edit_id'])){                                 
                    $db->delete('stores_users',['store_id'=>$_POST['edit_id']]);                    
                }                                
                $db->insert_bulk('stores_users',$user_store_conj);                                 

            }else{
                if(isset($_POST['edit_id'])){                                 
                    $db->delete('stores_users',['store_id'=>$_POST['edit_id']]);                    
                }                
            }

            $response['error'] = false;
            $response['message'] = (isset($_POST['edit_id'])) ? 'Details updated successfully' : 'Details added successfully';            
            $response['url'] = BASE_URL . 'admin/stores.php';            
            set_flash_session($response['error'],$response['message']);             
        }else{
            $response['error'] = true;
            $response['message'] ='Some fields are required !';
        }     
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    if($_POST['method']=='set_store'){
        if(isset($_POST['store_id']) && !empty($_POST['store_id'])){
            setcookie('store_id', $_POST['store_id'] , time() + (86400 * 30),'/');            
        }
    }
}


?>