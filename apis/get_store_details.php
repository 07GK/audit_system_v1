<?php

include '../class/class.php';
if(!is_user_logged_in()){
    move( BASE_URL . 'index.php');
}


## Read value
$draw = $_GET['draw'];
$row = $_GET['start'];
$rowperpage = $_GET['length']; // Rows display per page
$columnIndex = $_GET['order'][0]['column']; // Column index
$columnName = $_GET['columns'][$columnIndex]['data']; // Column name
$columnSortOrder = $_GET['order'][0]['dir']; // asc or desc
$searchValue = $_GET['search']['value']; // Search value

## Search 
$params = [];
$searchQueryArr = [];
$searchQuery = " ";

if($searchValue != ''){
   $searchQuery = " and ( s.name like ? or s.address like ? ) ";   
   $searchQueryArr = array_fill(0, 2, "%{$searchValue}%");   
}

if(isset($_GET['user_id']) && !empty($_GET['user_id']) ){
  $filter_user_wise = " and usj.user_id = ?"; 
  $params[] = $db->decrypt($_GET['user_id']);
}

## Total number of records without filtering
$records = $db->get_single("select count(*) as allcount from stores s where s.is_active = 1");
$totalRecords = $records['allcount'];

## Total number of record with filtering
$params = array_merge($params,$searchQueryArr);
$records = $db->get_single("select count(distinct s.id) as allcount from stores s left join stores_users usj on usj.store_id=s.id where s.is_active = 1 ".$searchQuery. $filter_user_wise ,$params);
$totalRecordwithFilter = $records['allcount'];

## Fetch records
array_push($params,$row,$rowperpage);
$storeRecords = $db->get_all("select s.*, count(Distinct u.id)  as user_cnt from stores s left join stores_users usj on usj.store_id=s.id left join users u on u.id = usj.user_id  where s.is_active = 1 ".$searchQuery." ". $filter_user_wise ." group by s.id  order by id desc limit ?,? ",$params);

$data = array();
$i=1;
foreach ($storeRecords as $row) {
    $operate = '<a href="'.BASE_URL . 'admin/add_store.php?edit_id='. $db->encrypt($row['id']) .'" class="btn btn-primary btn-xs mr-1 mb-1" title="Edit" ><i class="fa fa-pen" ></i></a>';
    $operate .= '<a href="'.BASE_URL . 'admin/users.php?store_id='.$db->encrypt($row['id']) .'" class="btn btn-warning btn-xs mr-1 mb-1" title="View the attached users" ><i class="fa fa-eye" ></i></a>';
    $operate .= '<a href="javascript:void(0)" class="btn btn-danger btn-xs mr-1 mb-1 delete" data-id="'.$db->encrypt($row['id']).'" data-table="stores" ><i class="fa fa-trash" title="Delete" ></i></a>';

    $data[] = array($i,htmlspecialchars($row['name'], ENT_QUOTES, 'UTF-8'),htmlspecialchars($row['address'], ENT_QUOTES, 'UTF-8'),$row['user_cnt'],$operate);
    $i++;
}

## Response
$response = array(
  "draw" => intval($draw),
  "iTotalRecords" => $totalRecords,
  "iTotalDisplayRecords" => $totalRecordwithFilter,
  "aaData" => $data,        
);

header('Content-Type: application/json');
echo json_encode($response);



?>
