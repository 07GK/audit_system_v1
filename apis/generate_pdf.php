<?php

include '../class/class.php';
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    header('Content-type: application/pdf');
    header('Content-Disposition: attachment; filename="report.pdf"');
    

    class MYPDF extends TCPDF {

        //Page header
        public function Header() {            
            if($this->page > 1){
                $this->SetFont('helvetica', 'B', 20);
                $this->SetY(10);
                // Title
                $this->Cell(0, 15, 'Audit Reports', 0, false, 'C', 0, '', 0, false, 'M', 'centerB');
            }
        }
    
        // Page footer
        public function Footer() {
            if($this->page > 1){
                // Position at 15 mm from bottom
                $this->SetY(-15);
                // Set font
                $this->SetFont('helvetica', 'I', 8);
                // Page number
                $this->Cell(0, 10, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
            }
        }
    }



    ob_start();
    $userdata = json_decode($_COOKIE['user_data'], true);
    $user_id = (isset($userdata['id']))?$userdata['id']:$_SESSION['user_data']['id'];    
    $store_id = $_COOKIE['store_id'];    

    $betweenDate = '';    
    $filter = [];
    // print_r($_POST);
    if (isset($_POST['start_date']) && isset($_POST['end_date']) && !empty($_POST['start_date']) &&  !empty($_POST['end_date'])) {
        $betweenDate = " and ar.date BETWEEN ? and ? ";       
        array_push($filter,date_format(date_create($_POST['start_date']), "Y-m-d"),date_format(date_create($_POST['end_date']), "Y-m-d"));        
    }
    if(isset($_POST['store_id']) && !empty(trim($_POST['store_id'])) && is_numeric($_POST['store_id']) ){
        $searchQuery.=" and ar.store_id=? ";
        $store_id = $_POST['store_id'];        
        array_push($filter,$store_id);    
    }
  
    if(isset($_POST['user_id']) && !empty(trim($_POST['user_id'])) && is_numeric($_POST['user_id']) ){
        $searchQuery.=" and ar.user_id=? ";
        $user_id = $_POST['user_id'];        
        array_push($filter,$user_id);    
    }  
    
        
    if(is_admin()){                
        
        $storeRecords = $db->get_all("select ar.*,s.name as store_name,r.remarks as remarks from audit_records ar inner join stores s on s.id = ar.store_id left join remarks r on r.id=ar.remark_id where 1 " . $betweenDate . $searchQuery." order by id desc ",$filter);            
    }else{
        array_push($filter,$store_id,$user_id);    
        print_r($filter);        
        $storeRecords = $db->get_all("select ar.*,s.name as store_name,r.remarks as remarks from audit_records ar inner join stores s on s.id = ar.store_id left join remarks r on r.id=ar.remark_id where 1 " . $betweenDate . " and ar.store_id = ?  and ar.user_id = ?  order by id desc ",$filter);    
    }
    
    if(!empty($storeRecords)){

        $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);


        // enable header and footer
        $pdf->setPrintHeader(true);
        $pdf->setPrintFooter(true);

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);


        // set font
        $pdf->SetFont('helvetica', '', 10);

        // add a page
        $pdf->AddPage();
        $pdf->SetXY(0, 0);
        $style = array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => '10,20,5,10', 'phase' => 10, 'color' => array(255, 0, 0));
        $pdf->Rect($pdf->getPageWidth() - 30, 0, 30, $pdf->getPageHeight(), 'DF', $style, array(125, 125, 131));

        $pdf->SetXY(0, 0);
        $style = array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => '10,20,5,10', 'phase' => 10, 'color' => array(255, 0, 0));
        $pdf->Rect($pdf->getPageWidth() - 50, 0, 8, $pdf->getPageHeight(), 'DF', $style, array(6, 75, 203));

        $pdf->SetXY(0, 0);
        $style = array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => '10,20,5,10', 'phase' => 10, 'color' => array(255, 0, 0));
        $pdf->Rect($pdf->getPageWidth() - 65, 0, 8, $pdf->getPageHeight(), 'DF', $style, array(6, 75, 203));

        $pdf->Image( BASE_URL.'assets/img/logo.png', 50, 70, 40, 40, '', '', 'T', false, 300, '', false, false, false, false, false, false);
        $pdf->Ln(45);
        $pdf->SetFont('helvetica', 'B', '45');
        $pdf->Cell(110, 10, 'Global Vigilence', 0, 1, 'C', 0, '', 1);
        $pdf->Ln(5);
        $pdf->SetFont('helvetica', 'U', '25');
        $pdf->Cell(110, 10, 'CCTV Video Auditing Service Provider', 0, 1, 'C', 0, '', 1);

        $pdf->Ln(55);
        $pdf->SetFont('helvetica', 'B', '25');
        $pdf->Cell(110, 10, 'For,', 0, 1, 'L', 0, '', 1);
        $pdf->Ln(5);
        $pdf->Cell(110, 10, ''.$storeRecords[0]['store_name'].'', 0, 1, 'C', 0, '', 1);
        $pdf->Ln(5);
        if (isset($_POST['start_date']) && isset($_POST['end_date']) && !empty($_POST['start_date']) &&  !empty($_POST['end_date'])) {
            $pdf->Cell(110, 10, '('.date_format(date_create($_POST['start_date']), "M d Y").') to ('.date_format(date_create($_POST['end_date']), "M d Y").')', 0, 1, 'L', 0, '', 1);
        }
        $i = 0;
        
        foreach ($storeRecords as $row) {
            if($i % 3 == 0){
                $pdf->AddPage();
            }
            // Table with rowspans 
            $tbl = '
            <table border="1" style="font-size:16px;  border-collapse: collapse;">
            <tr>
            <td width="300" align="left"><b>Date</b> : '.$row['date'].' </td>
            <td width="300" align="left"><b>Time</b> : '.$row['time'].' </td>  
            </tr>
            <tr>
                <td width="600" align="left" colspan="2"><b>Remarks</b> : '.$row['remarks'].' </td>  
            </tr>        
            <tr>
                <td width="600" align="left" colspan="2">
                    <table cellpadding="10">
                        <tr>
                            <td height="200" align="center"><img src= "'. BASE_URL. trim($row['image_path']).'" height="200"> </td>
                        </tr>
                    </table>
                </td>            
            </tr>
            </table>
            ';
            
            $pdf->writeHTML($tbl, true, false, false, false, '');
            $i++;
        }

        ob_clean();    

        return $pdf->Output('report.pdf', 'I');
    }else{
        return false;
    }

}
http_response_code(405);
exit();
?>