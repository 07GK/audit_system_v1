<?php

include '../class/class.php';
if(!is_user_logged_in()){
    move( BASE_URL . 'index.php');
}

## Read value
$draw = $_GET['draw'];
$row = $_GET['start'];
$rowperpage = $_GET['length']; // Rows display per page
$columnIndex = $_GET['order'][0]['column']; // Column index
$columnName = $_GET['columns'][$columnIndex]['data']; // Column name
$columnSortOrder = $_GET['order'][0]['dir']; // asc or desc
$searchValue = $_GET['search']['value']; // Search value

## Search 
$searchQuery = " ";
$searchQueryArr = [];
if($searchValue != ''){
   $searchQuery = " and ( remarks like ? )";   
   $searchQueryArr[] = "%{$searchValue}%";
}

if(isset($_GET['selected_store']) && !empty($_GET['selected_store']) && is_numeric($_GET['selected_store']) ){
  $searchQuery .= " and r.store_id = ?";
  $searchQueryArr[] = $_GET['selected_store'];
  
}

## Total number of records without filtering
$records = $db->get_single("select count(*) as allcount from remarks");
$totalRecords = $records['allcount'];

## Total number of record with filtering
$params = $searchQueryArr;

if(isset($_GET['flag']) && trim($_GET['flag']) =='get_request' ){  
  $filter1 = array_merge(array('1','7'),$params);    
  $records = $db->get_single("select count(*) as allcount from remarks r where r.is_custom=? and r.is_active!=? ".$searchQuery,$filter1);  
}else{
  $filter2 = array_merge(array('0','1'),$params);    
  // print_r($params);
  $records = $db->get_single("select count(*) as allcount from remarks r where r.is_custom=? and r.is_active!=? ".$searchQuery,$filter2);  
}
$totalRecordwithFilter = $records['allcount'];
## Fetch records
if(isset($_GET['flag']) && trim($_GET['flag']) =='get_request' ){    
  $filter1  = array_merge($filter1,array($row,$rowperpage));

  $storeRecords = $db->get_all("select r.*,s.name as store_name from remarks r left join stores s on r.store_id = s.id  where r.is_custom=? and r.is_active!=? ".$searchQuery." order by id  desc limit ?,?",$filter1);  
}else{  
  $filter2 = array_merge($filter2,array($row,$rowperpage));  
  $storeRecords = $db->get_all("select r.*,s.name as store_name from remarks r left join stores s on r.store_id = s.id where r.is_custom=? and r.is_active=? ".$searchQuery."  order by id desc limit ?,?",$filter2);
}
$data = array();
$i=1;
foreach ($storeRecords as $row) {
  if(isset($_GET['flag']) && trim($_GET['flag']) =='get_request' ){    
    $operate = '<a href="javascript:void(0)" class="btn btn-primary btn-xs mr-1 mb-1 made_request" data-action="approve" data-id="'.$row['id'].'" title="Approve the request" ><i class="fa fa-plus" ></i></a>';
    $operate .= '<a href="javascript:void(0)" class="btn btn-danger btn-xs mr-1 mb-1 made_request" data-action="reject" data-id="'.$row['id'].'" title="Reject the request" ><i class="fa fa-times" ></i></a>';    
    
  }else{    
    $operate = '<a href="'.BASE_URL . 'admin/add_remark.php?edit_id='. $db->encrypt($row['id']) .'" class="btn btn-primary btn-xs mr-1 mb-1" ><i class="fa fa-pen" ></i></a>';
    $operate .= '<a href="javascript:void(0)" class="btn btn-danger btn-xs mr-1 mb-1 delete" data-id="'.$db->encrypt($row['id']).'" data-table="remarks" ><i class="fa fa-trash" ></i></a>';
  }
    $data[] = array($i,htmlspecialchars(ucfirst($row['store_name'])),htmlspecialchars(ucfirst($row['remarks']), ENT_QUOTES, 'UTF-8'),$operate);
  $i++;
}

## Response
$response = array(
  "draw" => intval($draw),
  "iTotalRecords" => $totalRecords,
  "iTotalDisplayRecords" => $totalRecordwithFilter,
  "aaData" => $data
);

header('Content-Type: application/json');
echo json_encode($response);

?>