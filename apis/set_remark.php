<?php
include '../class/class.php';
if(!is_user_logged_in()){
    move( BASE_URL . 'index.php');
}


if(isset($_POST)){

    if(isset($_POST['edit_id'])){
        $_POST['edit_id'] = $db->decrypt($_POST['edit_id']);
        if($db->is_exist('remarks',['remarks'=>trim($_POST['remark']),'store_id'=>$_POST['store_id']],trim($_POST['edit_id']))){
            $response['error'] = true;
            $response['message'] = 'Remark already exist!';
            header('Content-Type: application/json');
            echo json_encode($response);
            return false;            
        }        
    }else{
        
        if($db->is_exist('remarks',['remarks'=>trim($_POST['remark']),'store_id'=>$_POST['store_id']])){        
            $response['error'] = true;
            $response['message'] = 'Remark already exist!';
            header('Content-Type: application/json');
            echo json_encode($response);
            return false;
        }        
    }    

    
    $_POST = array_map('trim', $_POST);        
    
    if(!empty($_POST['remark'])){            
        $tmp = [
            'store_id'=>$_POST['store_id'],
            'remarks'=>filter_var($_POST['remark'], FILTER_SANITIZE_STRING),  
            'is_active'=>'1',  
        ];                                    
        if(isset($_POST['edit_id'])){            
            $db->update('remarks',$tmp,['id'=>$_POST['edit_id']]);
            $id = $_POST['edit_id'];
        }else{                    
            $id = $db->insert('remarks',$tmp);            
        }
    
        $response['error'] = false;
        $response['message'] = (isset($_POST['edit_id'])) ? 'Details updated successfully' : 'Details added successfully';
        $response['url'] = BASE_URL . 'admin/remarks.php';            
        set_flash_session($response['error'],$response['message']);  
    }else{
        $response['error'] = true;
        $response['message'] ='Remark field is required !';
    } 
    header('Content-Type: application/json');
    echo json_encode($response);

}


?>