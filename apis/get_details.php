<?php
    include '../class/class.php';
    if(isset($_POST)){    
        
        $flag = 0;
        
        switch (trim($_POST['method'])) {
            case 'get_store_wise_user':                                                
                $flag = 1;
                if(!empty($_POST['id']) && is_numeric($_POST['id'])){
                    $data = $db->get_all("select distinct u.id,u.first_name,u.last_name from users u inner join stores_users usj on usj.user_id=u.id where usj.store_id = ? and u.is_active=1",[$_POST['id']]);                                                            
                }else{                    
                    $data = $db->get_all("select * from users where is_active=1");                    
                }
            break;   
            case 'get_store_wise_remarks':
                $flag = 1;
                if(!empty($_POST['id']) && is_numeric($_POST['id'])){
                    $data = $db->get_all("select r.* from remarks r inner join stores s on s.id = r.store_id where r.store_id = ? and r.is_custom='0' and r.is_active='1' ",[$_POST['id']]);                                                            
                }else{                    
                    $data = $db->get_all("select * from remarks");                    
                }
            break;         
            default:                
                break;
        }

        if(!empty($data) && $flag == 1 ){
            $userdata = json_decode($_COOKIE['user_data'], true);
            $user_id = (isset($userdata['id']))?$userdata['id']:$_SESSION['user_data']['id'];   

            $response['error'] = false;
            $response['message'] = 'Data retrieved successfully ';
            $response['data']['result'] = $data;
            $response['data']['id'] = ( is_admin() ) ? $user_id : 0;
        }else{
            $response['error'] = true;
            $response['message'] = ' No data found ';
            $response['data']['id'] = ( is_admin() ) ? $user_id : 0;
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }
?>