<?php

include '../class/class.php';
if(!is_user_logged_in()){
    move( BASE_URL . 'index.php');
}


## Read value
$draw = $_GET['draw'];
$row = $_GET['start'];
$rowperpage = $_GET['length']; // Rows display per page
$columnIndex = $_GET['order'][0]['column']; // Column index
$columnName = $_GET['columns'][$columnIndex]['data']; // Column name
$columnSortOrder = $_GET['order'][0]['dir']; // asc or desc
$searchValue = $_GET['search']['value']; // Search value

## Search 
$params = [];
$searchQueryArr = [];
$searchQuery = " ";

if($searchValue != ''){
   $searchQuery = "and ( u.first_name like ? or u.last_name like ? or u.email like ?) ";
   $searchQueryArr = array_fill(0, 3, "%{$searchValue}%");   
}
if(isset($_GET['store_id']) && !empty($_GET['store_id']) ){
  $filter_store_wise = " and usj.store_id= ?";
  $params[] = $db->decrypt($_GET['store_id']);    
}

## Total number of records without filtering
$records = $db->get_single("select count(*) as allcount from users u where u.is_active=1");
$totalRecords = $records['allcount'];

## Total number of record with filtering
$params = array_merge($params,$searchQueryArr);
$records = $db->get_single("select count(DISTINCT u.id) as allcount from users u  left join stores_users usj on usj.user_id=u.id where u.is_active=1 ".$searchQuery." ".$filter_store_wise,$params);
$totalRecordwithFilter = $records['allcount'];

## Fetch records
array_push($params,$row,$rowperpage);
$storeRecords = $db->get_all("select u.*, COUNT(DISTINCT s.name) as store_cnt from users u left join stores_users usj on usj.user_id=u.id left join stores s on s.id = usj.store_id inner join roles r on r.user_id !=u.id   where u.is_active=1 ".$searchQuery." ".$filter_store_wise." group by u.id order by id desc limit ?,?",$params);

$data = array();
$i=1;
foreach ($storeRecords as $row) {
    $operate = '<a href="'.BASE_URL . 'admin/add_user.php?edit_id='.$db->encrypt($row['id']) .'" class="btn btn-primary btn-xs mr-1 mb-1" title="Edit" ><i class="fa fa-pen" ></i></a>';
    $operate .= '<a href="'.BASE_URL . 'admin/stores.php?user_id='.$db->encrypt($row['id']) .'" class="btn btn-warning btn-xs mr-1 mb-1" title="View the attached stores" ><i class="fa fa-eye" ></i></a>';

    $operate .= '<a href="javascript:void(0)" class="btn btn-danger btn-xs mr-1 mb-1 delete" data-id="'.$db->encrypt($row['id']).'" data-table="users" title="Delete" ><i class="fa fa-trash" ></i></a>';
    $data[] = array($i,htmlspecialchars(ucfirst($row['first_name']), ENT_QUOTES, 'UTF-8'),htmlspecialchars(ucfirst($row['last_name']), ENT_QUOTES, 'UTF-8'),$row['email'],$row['store_cnt'],$operate);
    $i++;
}

## Response
$response = array(
  "draw" => intval($draw),
  "iTotalRecords" => $totalRecords,
  "iTotalDisplayRecords" => $totalRecordwithFilter,
  "aaData" => $data,
  
);

header('Content-Type: application/json');
echo json_encode($response);



?>
