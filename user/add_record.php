<?php
include '../class/class.php';
if (!is_user_logged_in()) {    
    header('location:'.BASE_URL . 'index.php');
}

if (is_user_logged_in() && is_admin()){    
    header('location:'.BASE_URL . 'admin/stores.php');
}

if (isset($_GET['edit_id'])) {

    $_GET['edit_id'] = $db->decrypt($_GET['edit_id']);
    if($db->is_exist('audit_records',['id'=>$_GET['edit_id']])){          
        $records_details = $db->get_single("select * from audit_records where id = ? and is_active=1",[$_GET['edit_id']]);                    
        $remarks_data = $db->get_all("select r.id,r.remarks from remarks r inner join stores s on s.id = r.store_id where r.store_id = ? and r.is_custom='0' ",[$records_details['store_id']]);                                                                    

    }else{
        header('location:records.php');
    }         
}else{
        $remarks_data = $db->get_all("select r.* from remarks r inner join stores s on s.id = r.store_id where r.is_custom='0' and r.store_id = ?",[$_COOKIE['store_id']]);
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= (isset($records_details)) ? 'Update Records' : 'Add Records'; ?></title>
    <?php
    include '../includes/include-css.php';
    ?>
</head>

<body class="hold-transition sidebar-mini layout-fixed ">
    <div class=" wrapper ">
        <?php include '../includes/sidebar.php';
        include '../includes/navbar.php';
        ?>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0">Manage Records</h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active">Records</li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->
            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    <div class='row'>
                        <div class="col-md-12">
                            <!-- general form elements -->
                            <div class="card card-primary">
                                <div class="card-header">
                                    <h3 class="card-title"><?= (isset($records_details)) ? 'Update Records' : 'Add Records'; ?></h3>
                                </div>
                                <!-- /.card-header -->
                                <form id="add_record" action='<?= BASE_URL . 'apis/set_record.php' ?>' method="post">
                                    <?php if (isset($records_details)) { ?>
                                        <input type='hidden' name="editId" value='<?= $records_details['id'] ?>'>
                                    <?php } ?>
                                    <div class="card-body">
                                        <div id='errorMessage' class='rounded p-1 m-1 text-center d-none col-6 m-auto'></div>
                                        <div class='row'>
                                        <div class="form-group col-md-6">
                                                <label for="date">Select Date *</label>
                                                <input type="date" name="date" class="form-control" value="<?= (isset($records_details)) ? $records_details['date'] : '' ?>">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="time">Select Time *</label>
                                                <input type="time" name="time" class="form-control" value="<?= (isset($records_details)) ? $records_details['time'] : '' ?>">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label for="remark">Remark *</label>                                            
                                                <select class="form-control multipleSelect remark" data-allow-clear='true' name='remark' id="selected_remarks" data-placeholder='Search and select users'>
                                                    <?php
                                                    if (!empty($remarks_data)) {                                                                                                            
                                                        foreach ($remarks_data as $row) {                                                                                                            
                                                            $selected = ($records_details['remark_id']==$row['id']) ? 'selected' : '';                                                            
                                                            echo '<option value="' . $row['id'] . '" '.$selected.' >' . $row['remarks'] . '</option>';                                                
                                                        }
                                                        echo '<option value="custom_remark">Custom remark</option>';
                                                    } else {
                                                        echo '<option>No remarks found</option>';
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-6 d-none custom_remark">
                                                <label for="time">Custom Remark *</label>
                                                <input type="text" class='form-control' name="custom_remark" value='' placeholder="Enter Custom Remark Here">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col">
                                                <label for="image">Upload Image *</label>
                                                <label class="text-danger img_required d-none">Image is required</label>
                                                <div id="upload_file" name="image" class="dropzone"></div>
                                            </div>
                                            <?php if(isset($records_details['id'])){ ?>
                                                    <div class="form-group col">                                                    
                                                        <label>Preview Image </label>                                                    
                                                        <div>
                                                            <img src="<?=BASE_URL. $records_details['image_path']?>" width='400'>
                                                        </div>
                                                    </div>  
                                                <?php } ?>  
                                        </div>                                                    
                                        
                                    </div>
                                    <!-- /.card-body -->
                                    <div class="card-footer">
                                        <button type="submit" id='submitBtn' class="btn btn-primary"><?= (isset($records_details)) ? 'Update Record' : 'Add Record'; ?></button>
                                    </div>
                                </form>
                            </div>
                            <!-- /.card -->
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <?php include '../includes/footer.php'; ?>
</body>
<?php include '../includes/include-script.php'; ?>

</html>