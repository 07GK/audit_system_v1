/*----------------------------------

Functions :-

1.loginForm    
2.formSubmitEvent

----------------------------------*/
var logged_in = 0;
const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 1000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
});


var url = window.location.origin + window.location.pathname;
var $selector = $('.sidebar a[href="' + url + '"]');
$($selector).addClass('active');
$($selector).closest('ul').closest('li').addClass('menu-open');
$($selector).closest('ul').removeAttr('style');
$($selector).closest('ul').closest('li').find('a[href*="#"').addClass('active');



//Add Record
$('.loginForm').validate({
    rules: {
        email: {
            required: true
        },
        password: {
            required: true,
            // minlength: 8
        },
    },
    submitHandler: function (form) {
        event.preventDefault();
        
        var submitBtn = $('.loginForm #submitBtn');    
        var error_box = $('.loginForm #errorMessage');    
        var button_text = submitBtn.html();
        var formData = new FormData(form);
        
        $.ajax({
            type: 'POST',
            url: $(form).attr('action'),
            data: formData,
            beforeSend: function() {
                submitBtn.html('Please Wait..');
                submitBtn.attr('disabled', true);
            },
            cache: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(result) {            
                if(result.error){
                    Toast.fire({icon: 'error',title: result.message});
                    // error_box.addClass("alert-danger").removeClass('d-none alert-success');
                }else{            
                    window.location.href = result.data.url;
                }
                // error_box.show().delay(1000).fadeOut();
                // error_box.html(result['message']);
                submitBtn.html(button_text);
                submitBtn.attr('disabled', false);
            }
        });
        
    }
});



//2.formSubmitEvent

function submit(form) {
event.preventDefault()  
var submitBtn = $('.formSubmitEvent #submitBtn');    
var error_box = $('.formSubmitEvent #errorMessage');    
var button_text = submitBtn.html();
var formData = new FormData(form);    

    $.ajax({
        type: 'POST',
        url: $(form).attr('action'),
        data: formData,
        beforeSend: function() {
            submitBtn.html('Please Wait..');
            submitBtn.attr('disabled', true);
        },
        cache: false,
        contentType: false,
        processData: false,
        dataType: 'json',
        success: function(result) {                              
            if(result.error){
                Toast.fire({icon: 'error',title: result.message});                
            }else{                            
                window.location.href = result.url;                
            }

            // $('.formSubmitEvent')[0].reset();
            // error_box.html(result['message']);
            submitBtn.html(button_text);
            submitBtn.attr('disabled', false);
        }
    });     
    
}

//Add Store
$("#add_store").validate({
    rules: {
        name: {
            required: true
        },
        address: {
            required: true
        },        
    },
    submitHandler: function (form) {
        submit(form);        
    }
});

//Add Remark
$("#add_remark").validate({
    rules: {
        remark: {
            required: true
        },
        store_id: {
            required: true
        }     
    },
    submitHandler: function (form) {
        submit(form);        
    }
});


//Add User
$("#add_user").validate({
    rules: {
        first_name: {
            required: true
        },
        last_name: {
            required: true
        },
        email: {
            required: true,
            email:true
        },
        password: {
            required: ($('input[name="edit_id"]').length > 0 ) ? false: true,
            minlength: 8
        }       
    },
    submitHandler: function (form) {
        submit(form);        
    }
});


$('.multipleSelect').select2({
    theme: 'bootstrap4',
    width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
    placeholder: $(this).data('placeholder'),
    allowClear: Boolean($(this).data('allow-clear')),    
});

$('.remark,.store_selection').select2({
    theme: 'bootstrap4',
    width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
    placeholder: $(this).data('placeholder'),
    allowClear: Boolean($(this).data('allow-clear')),    
});
// $('.remark,.store_selection').val(null).trigger('change');

$(document).ready(function(){    
    $('.dataTable').DataTable({
       'processing': true,
       'serverSide': true,
       'serverMethod': 'get',
       'ajax': {
           'url':base_url + $(this).find('table').data('url'),
           "data": function ( d ) {            
            d.start_date = ($('#start_date').val()) ? $('#start_date').val(): '';
            d.end_date = ($('#end_date').val()) ? $('#end_date').val() : '';            
            d.selected_store = ($('#selected_stores').val()) ? $('#selected_stores').val() : '';            
            d.selected_user = ($('#selected_users').val()) ? $('#selected_users').val() : '';                                   
        }
       }      
    });
});


var myDropzone;
Dropzone.autoDiscover = false;
if($("#upload_file").length > 0){
    
    myDropzone = new Dropzone("#upload_file", {
        url: base_url + "apis/set_record.php",
        autoProcessQueue: false,
        uploadMultiple: true,
        parallelUploads: 30,
        maxFilesize: 5,
        maxFiles: 1,
        addRemoveLinks: true,
        acceptedFiles: ".zip,.jpeg,.jpg,.png",
        init: function() {
            //Sending post data with file data
            this.on("sending", function(file, xhr, formData) {            
                formData.append('date', $('input[name="date"]').val());
                formData.append('time', $('input[name="time"]').val());
                formData.append('remark', $('.remark').val());
                formData.append('custom_remark', $('input[name="custom_remark"]').val());            
                formData.append('store_id', $('.select_stores').val());            
                if( typeof $('input[name="editId"]').val() !== 'undefined'){
                    formData.append('editId', $('input[name="editId"]').val());            
                    formData.append('remarkId', $('input[name="remarkId"]').val());            
                }
            })
    
        },
        complete:function(file) { 
            this.removeAllFiles(true); 
         },
        success: function(file, response) {        
            //After success get the response            
                // var error_box = $('#errorMessage');    
                if(response.error){
                    Toast.fire({icon: 'error',title: response.message});
                    // error_box.addClass("alert-danger").removeClass('d-none alert-success');
                }else{                            
                    window.location.href = response.url;                
                }
                // error_box.html(response['message']);
                // $('#add_record')[0].reset();
            
        }
    });

}

//Add Record
$('#add_record').validate({
    rules: {
        date: {
            required: true
        },
        time: {
            required: true
        },
        remark: {
            required: true
        },        
        image: {
            required: false
        },
        store_id: {
            required: ($('.select_stores').length > 0) ? true : false
        }
    },
    submitHandler: function (form) {
        event.preventDefault();
        if(myDropzone.files.length > 0){
            myDropzone.processQueue();            
        }else{
            submit(form);        
            // if(){
            //     $('.img_required').removeClass('d-none');
            // }
        }
        
    }
});

$(document).on('select2:select', '.remark', function (e) {
    var data = e.params.data;    
    if(data.id=="custom_remark"){
        $('.custom_remark').removeClass('d-none');
    }else{
        $('.custom_remark').addClass('d-none');
    }
});

$(document).on('select2:unselect', '.remark', function (e) {    
    $('.custom_remark').addClass('d-none');    
});

$(document).on('change', '.store_selection', function (e) {
    $.ajax({
        type: 'POST',
        url: base_url + 'apis/set_store.php',
        data: {store_id : $(this).val(),method:'set_store'},   
        success:function(){
            $('.dataTable').DataTable().ajax.reload();
        }     
    }); 
});

$('#datepicker').attr({ 'placeholder': ' Select Date Of Ranges To Filter ', 'autocomplete': 'off' });
$('#datepicker').on('cancel.daterangepicker', function (ev, picker) {
    $(this).val('');
    $('#start_date').val('');
    $('#end_date').val('');
});
$('#datepicker').on('apply.daterangepicker', function (ev, picker) {
    var drp = $('#datepicker').data('daterangepicker');
    $('#start_date').val(drp.startDate.format('YYYY-MM-DD'));
    $('#end_date').val(drp.endDate.format('YYYY-MM-DD'));
    $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
});

$('#datepicker').daterangepicker({
    showDropdowns: true,
    alwaysShowCalendars: true,
    autoUpdateInput: false,    
    ranges: {
        'Today': [moment(), moment()],
        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month': [moment().startOf('month'), moment().endOf('month')],
        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    },
    startDate: moment().subtract(29, 'days'),
    endDate: moment(),
    locale: {
        "format": "DD/MM/YYYY hh:mm A",
        "separator": " - ",
        "cancelLabel": 'Clear',
        'label': 'Select range of dates to filter'
    }
});

function date_wise_search(){
    $('.dataTable').DataTable().ajax.reload();
}


$(document).on('click','.made_request',function(){
    var id = $(this).data('id');    
    var action = $(this).data('action');    
    $.ajax({
        type: 'POST',
        url: base_url + "apis/update_remarks_request.php",
        data: {id : id,action:action},        
        success: function(result) {            
            $('.dataTable').DataTable().ajax.reload();          
        }
    });  
})

$(document).on('click','.delete',function()
{   
    var table = $(this).data('table');
    var id = $(this).data('id');
    var text = '';
    if(table=='stores'){
        text = 'Want to delete <br><span class="text-danger">'+$(this).closest('tr').find('td:eq(1)').html()+'</span><br>store ?';
    }
    if(table=='users'){
        text = 'Want to delete <br><span class="text-danger">'+$(this).closest('tr').find('td:eq(1)').html()+' '+$(this).closest('tr').find('td:eq(2)').html()+'</span><br>user ?';
    }
    if(table=='audit_records'){
        text = 'Want to delete the record ?';
    }
    if(table=='remarks'){
        text = 'Want to delete the remark ?';
    }
    Swal.fire({
        title: 'Are you sure.<br>'+text,
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: 'POST',
                url: base_url + "apis/delete_data.php",
                data: {id : id,table:table},        
                success: function(result) {            
                    $('.dataTable').DataTable().ajax.reload();          
                }
            });           
        }
      });

});

$(document).on('click','#download_pdf',function(){    
    var start_date = $('#start_date').val();
    var end_date = $('#end_date').val();
    var store_id = $('#selected_stores').val();
    var user_id = $('#selected_users').val();    
    
      $.ajax({
        url:  base_url + "apis/generate_pdf.php",
        method:'POST',   
        cache:false,
        data:{start_date:start_date,end_date:end_date,store_id:store_id,user_id:user_id},
        xhrFields:{
            responseType: 'blob'
        },     
        success: function(data) {                        
            if(data.size < 10){
                Toast.fire({icon: 'error',title: 'No records found !'});
            }else{
                var blob = new Blob([data], { type: 'application/pdf' });
                var link=document.createElement('a');
                link.href=window.URL.createObjectURL(blob);
                link.download="report.pdf";
                link.click();
            }         
        }
      });
});  

// add paste event listener to the page
document.onpaste = function(event){
  var items = (event.clipboardData || event.originalEvent.clipboardData).items;
  for (index in items) {
    var item = items[index];
    if (item.kind === 'file') {
      // adds the file to your dropzone instance      
      if(myDropzone.files.length > 0){
            myDropzone.removeAllFiles();      
            myDropzone.addFile(item.getAsFile());       
        }else{
            myDropzone.addFile(item.getAsFile());       
        }   
    }
  }
}

$(document).on('change','.select_stores,.store_selection',function(){
    var id = $(this).val();
    var method = $(this).data('method');
    
    $.ajax({
        type: 'POST',
        url: base_url + "apis/get_details.php",
        data: {id : id,method : method},        
        success: function(result) {      
            if( $('#selected_users').length > 0 ){
                if(!result.error){
                    var select = '<option value="all">All</option>';
                    for (let i = 0; i < result.data.result.length; i++) {
                        var admin_flag = (result.data.id!=0 &&  result.data.id== result.data.result[i]['id'] ) ? ' ( Admin ) ' : '';
                        select += '<option value="'+result.data.result[i]['id']+'" >'+result.data.result[i]['first_name']+' '+result.data.result[i]['last_name']+admin_flag+'</option>';
                    }     
                    $('.select_users').html(select);
                }else{
                    $('.select_users').html('<option value=" ">No user found </option>');
                }
            }
            if( $('#selected_remarks').length > 0 ){
                if(!result.error){  
                    select = '';
                    for (let i = 0; i < result.data.result.length; i++) {
                        var admin_flag = (result.data.id!=0 &&  result.data.id== result.data.result[i]['id'] ) ? ' ( Admin ) ' : '';
                        select += '<option value="'+result.data.result[i]['id']+'" >'+result.data.result[i]['remarks']+'</option>';
                    }     
                    
                }else{
                    select +='<option value=" ">No remarks found </option>';                    
                }   
                if(result.data.id==0){
                    select +='<option value="custom_remark">Custom remark</option>';
                }
                $('#selected_remarks').html(select);
            }
        }
    });      
    
});