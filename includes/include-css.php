<!-- Google Font: Source Sans Pro -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
<!-- Font Awesome -->
<link rel="stylesheet" href="<?=BASE_URL.'assets/css/fontawesome-free/css/all.min.css'?>">
<!-- icheck bootstrap -->
<link rel="stylesheet" href="<?=BASE_URL.'assets/css/icheck-bootstrap/icheck-bootstrap.min.css'?>">
<!-- Theme style -->
<link rel="stylesheet" href="<?=BASE_URL.'assets/css/dist/adminlte.min.css'?>">    
<!-- Date & Range -->
<link rel="stylesheet" type="text/css" href="<?= BASE_URL.'assets/css/daterangepicker/daterangepicker.css'?>" />
<!-- Select2 -->
<link rel="stylesheet" href="<?=BASE_URL.'assets/css/select2/select2.min.css'?>">
<link rel="stylesheet" href="<?=BASE_URL.'assets/css/select2-bootstrap4-theme/select2-bootstrap4.min.css'?>">
<!-- Datatables -->
<link rel="stylesheet" href="<?=BASE_URL.'assets/css/datatables/jquery.dataTables.min.css'?>">
<!-- DropZone js -->
<link rel="stylesheet" type="text/css" href="<?=BASE_URL.'assets/css/dropzone/dropzone.css'?>" />

<script src="<?=BASE_URL.'assets/js/sweetalert2/sweetalert2.min.js'?>"></script>
<link rel="stylesheet" href="<?=BASE_URL.'assets/css/sweetalert2/sweetalert2.min.css'?>" id="theme-styles">

<link rel="shortcut icon" href="<?=BASE_URL.'assets/img/logo.png'?>" type="image/x-icon">
<style>
    .error{
        color:red;
    }
</style>
<script>
    base_url = "<?=BASE_URL?>";
</script>